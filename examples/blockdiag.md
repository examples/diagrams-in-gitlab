# Block Diagram (blockdiag)
The following plot is created using [Blockdiag](https://github.com/blockdiag/blockdiag).

```blockdiag
blockdiag {
  A -> B -> C -> D;
  A -> E -> F -> G;
}
```
