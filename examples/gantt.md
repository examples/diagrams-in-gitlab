# Gantt Diagram
A Gantt diagram is a type of a bar chart illustrating a project schedule.
It can also show the dependency relationships between activities.
A complete reference for creating Gantt diagrams is available in the [documentation](https://plantuml.com/gantt-diagram).
<details>
  <summary>Show code</summary>
  <div>

    ```plantuml
    @startgantt
    printscale weekly

    Project starts the 1st of July 2021
    [Literature Review] lasts 4 weeks
    [Literature Review] is 100% completed
    then [Run Experiments] lasts 12 weeks
    [Run Experiments] is 70% completed
    -- Phase Two --
    then [Data Analysis] lasts 8 weeks
    [Data Analysis] is 0% completed
    then [Write Publication] lasts 6 weeks
    [Write Publication] is 0% completed
    @endgantt
    ```

  </div>
</details>

```plantuml
@startgantt
printscale weekly

Project starts the 1st of July 2021
[Literature Review] lasts 4 weeks
[Literature Review] is 100% completed
then [Run Experiments] lasts 12 weeks
[Run Experiments] is 70% completed
-- Phase Two --
then [Data Analysis] lasts 8 weeks
[Data Analysis] is 0% completed
then [Write Publication] lasts 6 weeks
[Write Publication] is 0% completed
@endgantt
```
