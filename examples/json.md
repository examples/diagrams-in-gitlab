# Visualising JSON Data
Using PlantUML it is easy to visualize JSON data.
Please refer to the [documentation](https://plantuml.com/json) for a syntax
reference.
<details>
  <summary>Show code</summary>
  <div>

    ```plantuml
    @startjson
    {
      "firstName": "John",
      "lastName": "Smith",
      "age": 28,
      "address": {
        "streetAddress": "Bautzner Landstr. 400",
        "city": "Dresden",
        "state": "Saxony",
        "postalCode": "01328"
      },
      "offices": [
        {
          "building": 512,
          "room": 1234
        },
        {
          "building": 312,
          "room": 3
        }
      ],
      "departments": []
    }
    @endjson
    ```

  </div>
</details>

```plantuml
@startjson
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 28,
  "address": {
    "streetAddress": "Bautzner Landstr. 400",
    "city": "Dresden",
    "state": "Saxony",
    "postalCode": "01328"
  },
  "offices": [
    {
      "building": 512,
      "room": 1234
    },
    {
      "building": 312,
      "room": 3
    }
  ],
  "departments": []
}
@endjson
```
