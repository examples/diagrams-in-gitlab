# Mermaid Diagrams

[Mermaid](https://mermaid-js.github.io/mermaid) is a Javascript based charting tool, rendering diagrams from markdown-like syntax.

*Note:* Although the Kroki plugin also supports Mermaid, this Gitlab instance will instead plot Mermaid diagrams directly.


<details>
  <summary>Show code</summary>
  <div>
  
    ```mermaid
    sequenceDiagram
        participant Alice
        participant Bob
        Alice->>John: Hello John, how are you?
        loop Healthcheck
            John->>John: Fight against hypochondria
        end
        Note right of John: Rational thoughts <br/>prevail!
        John-->>Alice: Great!
        John->>Bob: How about you?
        Bob-->>John: Jolly good!
    ```  
  
  </div>
</details>


```mermaid
sequenceDiagram
    Alice->>Bob: Hello Bob!
    Bob-->>Alice: Fine!
    Alice->>Bob: How are you?
    Bob-->>Alice: Perfect!
    Alice-)Bob: See you!
```

Find more examples at: <https://mermaid-js.github.io/mermaid>