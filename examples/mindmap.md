# MindMap
Drawing MindMaps can be useful to visually organize information.
The hierarchical structure shows relationships among pieces of the whole.
A detailed introduction to MindMaps with PlantUML is available in the
[documentation](https://plantuml.com/mindmap-diagram).
<details>
  <summary>Show code</summary>
  <div>

    ```plantuml
    @startmindmap
    * Helmholtz Research Fields
    ** AWI
    ***_ Earth and Environment
    ** DESY
    ***_ Matter
    ** DKFZ
    ***_ Health
    ** DLR
    ***_ Energy
    ***_ Aeronautics, Space and Transport
    ** DZNE
    ***_ Health
    ** FZJ
    ***_ Energy
    ***_ Information
    ***_ Earth and Environment
    ***_ Matter
    ** GEOMAR
    ***_ Earth and Environment
    ** GSI
    ***_ Health
    ***_ Matter
    ** HZB
    ***_ Energy
    ***_ Information
    ***_ Matter
    ** HZDR
    ***_ Energy
    ***_ Health
    ***_ Matter
    ** ...
    @endmindmap
    ```

  </div>
</details>


```plantuml
@startmindmap
* Helmholtz Research Fields
** AWI
***_ Earth and Environment
** DESY
***_ Matter
** DKFZ
***_ Health
** DLR
***_ Energy
***_ Aeronautics, Space and Transport
** DZNE
***_ Health
** FZJ
***_ Energy
***_ Information
***_ Earth and Environment
***_ Matter
** GEOMAR
***_ Earth and Environment
** GSI
***_ Health
***_ Matter
** HZB
***_ Energy
***_ Information
***_ Matter
** HZDR
***_ Energy
***_ Health
***_ Matter
** ...
@endmindmap
```
