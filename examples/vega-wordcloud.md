# Vega Wordcloud

The following
wordcloud is generated from the
[mission statement of the Helmholtz Association](https://www.helmholtz.de/en/about-us/the-association/mission/)
using [Vega](https://vega.github.io/vega/).

<details>
  <summary>Show code</summary>
  <div>

    ```vega
    {
    "$schema": "https://vega.github.io/schema/vega/v5.json",
    "description": "A word cloud visualization depicting the mission statement of Helmholtz.",
    "width": 800,
    "height": 400,
    "padding": 0,

    "data": [
        {
        "name": "table",
        "values": [
            "We contribute to solving the major challenges facing society, science and the economy by conducting top-level research in strategic programmes within our six research fields: Energy, Earth & Environment, Health, Aeronautics, Space and Transport, Matter, and Information. We research highly complex systems using our large-scale devices and infrastructure, cooperating closely with national and international partners. We contribute to shaping our future by combining research and technology development with perspectives for innovative application and provisions in tomorrow's world. We attract and promote the best young talents, offering a unique research environment and general support throughout all career stages.",
            "The Helmholtz Association performs cutting-edge research which contributes substantially to solving the grand challenges of science, society and industry. Helmholtz Association scientists focus on researching the highly-complex systems which determine human life and the environment, for example: ensuring that society remains mobile, ensuring that society has a reliable energy supply, ensuring that future generations live in a secure ecosystem, and developing treatments for previously incurable diseases.",
            "The Helmholtz Association performs cutting-edge research which contributes substantially to solving the grand challenges of science, society and industry. Helmholtz Association scientists focus on researching the highly-complex systems which determine human life and the environment, for example: ensuring that society remains mobile, ensuring that society has a reliable energy supply, ensuring that future generations live in a secure ecosystem, and developing treatments for previously incurable diseases.",
            "The activities of the Helmholtz Association focus on securing the long-term foundations of human life and on creating the technological basis for a competitive economy. Our potential, as an Association, to achieve these goals is due to the outstanding scientists working at our 18 major research centres, a high-performance infrastructure and modern research management.",
            "Our scientists develop national research programmes for each of these fields and international experts review these programmes. These expert evaluations form the basis of the programme-oriented funding (POF) which finances to Helmholtz Association research.",
            "Within the six research fields, Helmholtz scientists cooperate with each other and with external partners - working across disciplines, organisations and national borders. Indeed, the being part of the Helmholtz Association means making concerted research effort in which networks form the key to inquiring thought and action. Concerted research is efficient and flexible. The Helmholtz Association uses this research to create an effective basis for shaping the future."
        ],
        "transform": [
            {
            "type": "countpattern",
            "field": "data",
            "case": "upper",
            "pattern": "[\\w']{3,}",
            "stopwords": "(i|me|my|myself|we|us|our|ours|ourselves|you|your|yours|yourself|yourselves|he|him|his|himself|she|her|hers|herself|it|its|itself|they|them|their|theirs|themselves|what|which|who|whom|whose|this|that|these|those|am|is|are|was|were|be|been|being|have|has|had|having|do|does|did|doing|will|would|should|can|could|ought|i'm|you're|he's|she's|it's|we're|they're|i've|you've|we've|they've|i'd|you'd|he'd|she'd|we'd|they'd|i'll|you'll|he'll|she'll|we'll|they'll|isn't|aren't|wasn't|weren't|hasn't|haven't|hadn't|doesn't|don't|didn't|won't|wouldn't|shan't|shouldn't|can't|cannot|couldn't|mustn't|let's|that's|who's|what's|here's|there's|when's|where's|why's|how's|a|an|the|and|but|if|or|because|as|until|while|of|at|by|for|with|about|against|between|into|through|during|before|after|above|below|to|from|up|upon|down|in|out|on|off|over|under|again|further|then|once|here|there|when|where|why|how|all|any|both|each|few|more|most|other|some|such|no|nor|not|only|own|same|so|than|too|very|say|says|said|shall)"
            },
            {
            "type": "formula", "as": "angle",
            "expr": "[-45, 0, 45][~~(random() * 3)]"
            },
            {
            "type": "formula", "as": "weight",
            "expr": "if(datum.text=='VEGA', 600, 300)"
            }
        ]
        }
    ],

    "scales": [
        {
        "name": "color",
        "type": "ordinal",
        "domain": {"data": "table", "field": "text"},
        "range": ["#005aa0", "#8cb423", "#5a696e"]
        }
    ],

    "marks": [
        {
        "type": "text",
        "from": {"data": "table"},
        "encode": {
            "enter": {
            "text": {"field": "text"},
            "align": {"value": "center"},
            "baseline": {"value": "alphabetic"},
            "fill": {"scale": "color", "field": "text"}
            },
            "update": {
            "fillOpacity": {"value": 1}
            },
            "hover": {
            "fillOpacity": {"value": 0.5}
            }
        },
        "transform": [
            {
            "type": "wordcloud",
            "size": [800, 400],
            "text": {"field": "text"},
            "rotate": {"field": "datum.angle"},
            "font": "Helvetica Neue, Arial",
            "fontSize": {"field": "datum.count"},
            "fontWeight": {"field": "datum.weight"},
            "fontSizeRange": [12, 56],
            "padding": 2
            }
        ]
        }
    ]
    }
    ```

  </div>
</details>

```vega
{
  "$schema": "https://vega.github.io/schema/vega/v5.json",
  "description": "A word cloud visualization depicting the mission statement of Helmholtz.",
  "width": 800,
  "height": 400,
  "padding": 0,

  "data": [
    {
      "name": "table",
      "values": [
        "We contribute to solving the major challenges facing society, science and the economy by conducting top-level research in strategic programmes within our six research fields: Energy, Earth & Environment, Health, Aeronautics, Space and Transport, Matter, and Information. We research highly complex systems using our large-scale devices and infrastructure, cooperating closely with national and international partners. We contribute to shaping our future by combining research and technology development with perspectives for innovative application and provisions in tomorrow's world. We attract and promote the best young talents, offering a unique research environment and general support throughout all career stages.",
        "The Helmholtz Association performs cutting-edge research which contributes substantially to solving the grand challenges of science, society and industry. Helmholtz Association scientists focus on researching the highly-complex systems which determine human life and the environment, for example: ensuring that society remains mobile, ensuring that society has a reliable energy supply, ensuring that future generations live in a secure ecosystem, and developing treatments for previously incurable diseases.",
        "The Helmholtz Association performs cutting-edge research which contributes substantially to solving the grand challenges of science, society and industry. Helmholtz Association scientists focus on researching the highly-complex systems which determine human life and the environment, for example: ensuring that society remains mobile, ensuring that society has a reliable energy supply, ensuring that future generations live in a secure ecosystem, and developing treatments for previously incurable diseases.",
        "The activities of the Helmholtz Association focus on securing the long-term foundations of human life and on creating the technological basis for a competitive economy. Our potential, as an Association, to achieve these goals is due to the outstanding scientists working at our 18 major research centres, a high-performance infrastructure and modern research management.",
        "Our scientists develop national research programmes for each of these fields and international experts review these programmes. These expert evaluations form the basis of the programme-oriented funding (POF) which finances to Helmholtz Association research.",
        "Within the six research fields, Helmholtz scientists cooperate with each other and with external partners - working across disciplines, organisations and national borders. Indeed, the being part of the Helmholtz Association means making concerted research effort in which networks form the key to inquiring thought and action. Concerted research is efficient and flexible. The Helmholtz Association uses this research to create an effective basis for shaping the future."
      ],
      "transform": [
        {
          "type": "countpattern",
          "field": "data",
          "case": "upper",
          "pattern": "[\\w']{3,}",
          "stopwords": "(i|me|my|myself|we|us|our|ours|ourselves|you|your|yours|yourself|yourselves|he|him|his|himself|she|her|hers|herself|it|its|itself|they|them|their|theirs|themselves|what|which|who|whom|whose|this|that|these|those|am|is|are|was|were|be|been|being|have|has|had|having|do|does|did|doing|will|would|should|can|could|ought|i'm|you're|he's|she's|it's|we're|they're|i've|you've|we've|they've|i'd|you'd|he'd|she'd|we'd|they'd|i'll|you'll|he'll|she'll|we'll|they'll|isn't|aren't|wasn't|weren't|hasn't|haven't|hadn't|doesn't|don't|didn't|won't|wouldn't|shan't|shouldn't|can't|cannot|couldn't|mustn't|let's|that's|who's|what's|here's|there's|when's|where's|why's|how's|a|an|the|and|but|if|or|because|as|until|while|of|at|by|for|with|about|against|between|into|through|during|before|after|above|below|to|from|up|upon|down|in|out|on|off|over|under|again|further|then|once|here|there|when|where|why|how|all|any|both|each|few|more|most|other|some|such|no|nor|not|only|own|same|so|than|too|very|say|says|said|shall)"
        },
        {
          "type": "formula", "as": "angle",
          "expr": "[-45, 0, 45][~~(random() * 3)]"
        },
        {
          "type": "formula", "as": "weight",
          "expr": "if(datum.text=='VEGA', 600, 300)"
        }
      ]
    }
  ],

  "scales": [
    {
      "name": "color",
      "type": "ordinal",
      "domain": {"data": "table", "field": "text"},
      "range": ["#005aa0", "#8cb423", "#5a696e"]
    }
  ],

  "marks": [
    {
      "type": "text",
      "from": {"data": "table"},
      "encode": {
        "enter": {
          "text": {"field": "text"},
          "align": {"value": "center"},
          "baseline": {"value": "alphabetic"},
          "fill": {"scale": "color", "field": "text"}
        },
        "update": {
          "fillOpacity": {"value": 1}
        },
        "hover": {
          "fillOpacity": {"value": 0.5}
        }
      },
      "transform": [
        {
          "type": "wordcloud",
          "size": [800, 400],
          "text": {"field": "text"},
          "rotate": {"field": "datum.angle"},
          "font": "Helvetica Neue, Arial",
          "fontSize": {"field": "datum.count"},
          "fontWeight": {"field": "datum.weight"},
          "fontSizeRange": [12, 56],
          "padding": 2
        }
      ]
    }
  ]
}
```
